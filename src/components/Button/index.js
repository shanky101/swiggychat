import React from 'react'
import classnames from 'classnames'
import PropTypes from 'prop-types'

export default class Button extends React.PureComponent {
    constructor(props) {
        super(props)
    }

    handleClick = (e) => {
        this.props.onClick && this.props.onClick(e)
    }

    render() {
        const { children, variant, size, onClick, disabled } = this.props;
        const buttonClassName = classnames(
            'button',
            {
                'is-small' : size === 'small',
                'is-medium' : size === 'medium',
                'is-large' : size === 'large',
                'is-primary' : variant === 'primary',
                'is-link' : variant === 'link',
                'is-info' : variant === 'info',
                'is-success' : variant === 'success',
                'is-warning' : variant === 'warning',
                'is-danger' : variant === 'danger',
                disabled
            },
            this.props.className
        )

        return(
            <button
                className={buttonClassName}
                onClick={this.handleClick}
            >
            {children}
            </button>
        )
    }
}

Button.propTypes = {
    onClick : PropTypes.func,
    children : PropTypes.any,
    size : PropTypes.string,
    variant : PropTypes.string,
    disabled : PropTypes.bool,
}