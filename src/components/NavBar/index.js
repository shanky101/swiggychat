import React from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';

export default class NavBar extends React.PureComponent {
    constructor(props) {
        super(props);
        
    }
    render() {
        const {children, logo, title, navLeft, navLeftDropdownTitle, navLeftDropdown, navRight} = this.props;
        return(
                <nav className='navbar is-info'>
                    <div className='navbar-brand'>
                        <a className='navbar-item' href='/'>
                            <h1 className='title'>{title}</h1>
                        </a>
                    <div className='navbar-burger burger' data-target='navbarExampleTransparentExample'>
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
            </div>

            <div className='navbar-menu'>
                <div className='navbar-start'>
                    {
                        navLeft && navLeft.map(item => {
                            return (
                                <a className='navbar-item' href={item.link}>
                                    {item.title}
                                </a>
                            )
                        })
                    }

                    <div className='navbar-item has-dropdown is-hoverable'>
                        <a className='navbar-link' href='/'>
                            {navLeftDropdownTitle}
                        </a>
                        <div className='navbar-dropdown is-boxed'>
                            {
                                navLeftDropdown && navLeftDropdown.map(item => {
                                    return(
                                        <a className='navbar-item' href={item.link}>
                                            {item.title}
                                        </a>
                                    )   
                                })
                            }
                        </div>
                    </div>

                </div>

                <div className='navbar-end'>
                    <div className='navbar-item'>
                        <div className='field is-grouped'>
                        {
                            navRight && navRight.map(item => {
                                return(
                                    <a className='navbar-item' href={item.link}>
                                        {item.title}
                                    </a>
                                )   
                            })
                        }
                        </div>
                    </div>
                </div>
            </div>
        </nav>
      );
    }
}

NavBar.propTypes = {
    children: PropTypes.any,
    title: PropTypes.string,
    navLeft: PropTypes.array,
    navLeftDropdownTitle: PropTypes.string,
    navLeftDropdown: PropTypes.array,
    navRight: PropTypes.array
}
