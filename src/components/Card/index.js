import React from 'react'
import classnames from 'classnames'
import PropTypes from 'prop-types'
import './_cardStyles.scss'

export default class Card extends React.PureComponent {
    constructor(props) {
        super(props)
    }

    handleClick = (e) => {
        this.props.onClick && this.props.onClick(e)
    }

    render() {
        const { children, size, title, description, variant, timeSlot } = this.props
        const cardClassName = classnames(
            'card',
            {
                'cardInfo' : variant === 'info',
                'cardDefault' : variant === 'default' || !variant
            },
            this.props.className
        )
        return(
            <div className={cardClassName} onClick={this.handleClick}>
                <div className='card-content'>
                    <div className='content'>
                        <p className='title is-spaced is-5 cardText'>
                            {title}
                        </p>
                        <p></p>
                        <p className='subtitle is-6 cardText'>
                            {description}
                        </p>
                        <p className='title is-spaced is-6 cardText'>
                            Posted At : {timeSlot}
                        </p>
                    </div>
                    <div>
                    </div>
                </div>
            </div>
        )
    }
}