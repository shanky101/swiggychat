import React from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';

export default class InputBox extends React.PureComponent {
    constructor(props) {
        super(props);
    }

    handleChange = (e) => {
        this.props.onChange && this.props.onChange(e);
    }

    render() {
        const { onChange, type, disabled, placeholder } = this.props;
        const inputBoxClassName = classnames(
            'input',
            this.props.className,
            { disabled }
        )
        return(
            <div className='control'>
                <input
                    placeholder={placeholder}
                    className={inputBoxClassName}
                    onChange={this.handleChange}
                    type={type}
                >
                </input>
            </div>

        );
    }
}

InputBox.propTypes = {
    disabled: PropTypes.bool,
    children: PropTypes.any,
    type: PropTypes.string,
    placeholder: PropTypes.string
}
