import {combineReducers} from 'redux'
import { } from '../actions'
import { initialCommentsState, ADD_COMMENT, DELETE_COMMENT, EDIT_COMMENT } from '../utils/constants'

function commentsReducer(state = initialCommentsState,action) {
    switch(action.type) {
      case ADD_COMMENT :
        console.log(action);
        return {
          ...state,
          comments: [
            ...state.comments,
            {
              comment: action.payload,
              id: action.id
            }
          ]
        }
      case DELETE_COMMENT :
          return {
            ...state,
            comments:
              state.comments.filter(item => {
                return item.id !== action.id
            })
          }    
      case EDIT_COMMENT :
          return {
            ...state,
            comments: 
              state.comments.map(item => (
                item.id === action.id ? 
                {
                  ...item,
                  comment: action.payload
                } : item
              ))
          }
      default: 
        return state;
    }
  }
  
  const rootReducer = combineReducers({
    commentsReducer
  });
  
  export default rootReducer;