export const ADD_COMMENT = 'ADD_COMMENT';
export const DELETE_COMMENT = 'DELETE_COMMENT';
export const EDIT_COMMENT = 'EDIT_COMMENT';
export const initialCommentsState = {
  comments: []
}; 

export const navleftData = [
    {
        title: 'Options',
        link: ''
    },
]

export const navRightData = [
    {
        title: 'Login',
        link: ''
    },
]

export const dropDownValues = [
    {
        title: 'Help',
        link: ''
    },
    {
        title: 'Friends',
        link: ''
    },
]