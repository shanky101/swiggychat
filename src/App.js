import React, { Component } from 'react';
import Home from './container/Home';
import './App.css';

class App extends Component {
  render() {
    return (
      <Home />
    );
  }
}

export default App;
