import React from 'react'
import uuid from 'uuid'
import classnames from 'classnames'
import PropTypes from 'prop-types'
import Clock from 'react-live-clock';
import { navleftData, navRightData, dropDownValues }  from '../../utils/constants';
import {connect} from 'react-redux'
import {addComment,editComment,deleteComment} from '../../actions'
import './_homeStyles.scss'

import NavBar from '../../components/NavBar';
import Card from '../../components/Card';
import InputBox from '../../components/InputBox';
import Button from '../../components/Button'


const mapStateToProps = state => {
    console.log('MapStateToProps', state.commentsReducer.comments);
    return {
        comments: state.commentsReducer.comments
    }
}
export class Home extends React.PureComponent {
    constructor(props) {
        super(props)

        this.state = {
            textBoxVal: '',
            time: ''
        }
    }

    handleTextChange = (e) => {
        console.log('Inside handletextchange')
        this.setState({textBoxVal: e.target.value})
    }

    handleDelete = (id) => {
        this.props.dispatch(deleteComment(id));
    }

    handleSubmit = (e) => {
        //e.preventDefault();
        console.log('Inside handle submit')
        const id = uuid();
        this.props.dispatch(addComment(id,this.state.textBoxVal));
        this.setState({time: <Clock format={'HH:mm:ss'} ticking={false} timezone={'Asia/Kolkata'} />})
    }

    handleClear = (e) => {
        this.state.textBoxValue &&
        console.log('Clear clicked');
        this.setState({textBoxValue: ''});
      }


    render() {
        const {comments } = this.props
        return(
            <div>
                <NavBar 
                    title='Swiggy Chat'
                    navLeft={navleftData}
                    navRight={navRightData}
                    navLeftDropdown={dropDownValues}
                />
                <div className='rootContainer'>
                    <Card
                        className='cardCommentStyle'
                        title='Lightning Fast Chat 📱'
                        description='Fast Texts, to all your buddies 😎'
                    > 
                    </Card>
                    <div className='commentsContainer'>
                        <InputBox 
                            type='input'
                            value={this.state.textBoxVal}
                            onChange={this.handleTextChange}
                        />
                        <Button
                            className='is-success'
                            onClick={this.handleSubmit}

                        >
                        Submit
                        </Button>
                    </div>
                    <div className='commentsContainer'>
                    {
                        <ol type={1}>
                        {
                            comments.map(item => (
                                <li key={item.id}>
                                <Card
                                    className='cardAdjust' 
                                    title={item.comment}
                                    timeSlot={this.state.time}
                                />
                                <Button
                                onClick={() => this.handleDelete(item.id)}
                                className='is-danger'
                                >
                                Delete
                                </Button>
                                </li>
                            ))
                        }
                        </ol>
                    }
                    </div>
                </div>
            </div>
        );
    }
}

export default Home = connect(mapStateToProps)(Home);


