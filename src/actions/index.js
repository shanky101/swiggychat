import { ADD_COMMENT, EDIT_COMMENT, DELETE_COMMENT } from '../utils/constants'

export function addComment(id, data) {
    console.log('Inside action', data)
    return {
        type: ADD_COMMENT,
        payload: data,
        id
    }
}

export function deleteComment(id) {
    return {
        type: DELETE_COMMENT,
        id
    }
}

export function editComment(id, data) {
    return {
        type: EDIT_COMMENT,
        payload: data,
        id
    }
}
